﻿using System.Linq;
using EloBuddy.SDK;

namespace AutoRift.Situation
{
    public static class Game
    {
        public static class Ally
        {
            public static int TotalKills { get { return EntityManager.Heroes.Allies.Sum(x => x.ChampionsKilled); } }
            public static float TotalGold { get { return EntityManager.Heroes.Allies.Sum(x => x.GoldTotal); } }
        }

        public static class Enemy
        {
            public static int TotalKills { get { return EntityManager.Heroes.Enemies.Sum(x => x.ChampionsKilled); } }
            public static float TotalGold { get { return EntityManager.Heroes.Enemies.Sum(x => x.GoldTotal); } }
        }
    }
}