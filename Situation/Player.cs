﻿using System;
using System.Linq;
using EloBuddy;
using EloBuddy.SDK;
using SharpDX;

// ReSharper disable ConvertPropertyToExpressionBody

namespace AutoRift.Situation
{
    public static class Player
    {
        public static AIHeroClient Hero { get { return EloBuddy.Player.Instance; } }
        public static Vector3 Posistion { get { return Hero.Position; } }
        //Regens
        public static float Health { get { return Hero.Health; } }
        public static float HealthPercent { get { return Hero.Health / Hero.MaxHealth; } }
        public static float Mana { get { return Hero.Mana; } }
        public static float ManaPercent { get { return Hero.Mana / Hero.MaxMana; } }
        public static float TotalShield { get { return Hero.AllShield; } }
        //Info
        public static float Level { get { return Hero.Level; } }
        public static float BoundingRadius { get { return Hero.BoundingRadius; } }
        public static float Gold { get { return Hero.Gold; } }
        //Damage
        public static float AttackDamage { get { return Hero.TotalAttackDamage; } }
        public static float AbilityPower { get { return Hero.TotalMagicalDamage; } }
        public static float LargestCrit { get { return Hero.LargestCriticalStrike; } }

        
        public static float DistanceTo(this AIHeroClient turrent, Vector3 pos)
        {
            return pos.Distance(turrent);
        }
        public static float DistanceToMe(this AIHeroClient turrent)
        {
            return Posistion.Distance(turrent);
        }
        public static bool InRegion(params Region.Location[] location)
        {
            return Region.IsInRegion(Posistion, location);
        }

        public static bool InAllyTurrent(params Turrent.Tier[] tiers)
        {
            return Posistion.IsInAllyTurrent(tiers);
        }
        public static bool InEnemyTurrent(params Turrent.Tier[] tiers)
        {
            return Posistion.IsInEnemyTurrent(tiers);
        }
        public static class Ally
        {
            public static float NearestDistance
            {
                get
                {
                    return
                        EntityManager.Heroes.Allies.Where(x => !x.IsDead)
                            .Min(ally => Hero.Position.Distance(ally.Position));
                }
            }
            public static float FurthestDistance
            {
                get
                {
                    return
                        EntityManager.Heroes.Allies.Where(x => !x.IsDead)
                            .Max(ally => Hero.Position.Distance(ally.Position));
                }
            }
            public static float NearestDistanceToTower
            {
                get
                {
                    return
                        EntityManager.Turrets.Allies.Where(x => !x.IsDead)
                            .Max(ally => Hero.Position.Distance(ally.Position));
                }
            }
        }

        public static class Enemy
        {
            public static float NearestDistance
            {
                get
                {
                    return
                        EntityManager.Heroes.Enemies.Where(x => !x.IsDead)
                            .Min(ally => Hero.Position.Distance(ally.Position));
                }
            }
            public static float FurthestDistance
            {
                get
                {
                    return
                        EntityManager.Heroes.Enemies.Where(x => !x.IsDead)
                            .Max(ally => Hero.Position.Distance(ally.Position));
                }
            }
            public static float NearestDistanceToTower
            {
                get
                {
                    return
                        EntityManager.Turrets.Enemies.Where(x => !x.IsDead)
                            .Max(ally => Hero.Position.Distance(ally.Position));
                }
            }
        }

    }
}