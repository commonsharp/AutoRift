﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoRift.Core;
using EloBuddy;
using EloBuddy.SDK;
// ReSharper disable ConvertPropertyToExpressionBody

namespace AutoRift.Situation
{
    public static class Enemy
    {
        public static List<AIHeroClient> Heroes { get { return EntityManager.Heroes.Enemies; } }
        public static Dictionary<AIHeroClient, SpellAvaliblity> SpellAvaliblities = new Dictionary<AIHeroClient, SpellAvaliblity>(); 

        static Enemy ()
        {
            foreach (var enemy in Heroes)
            {
                SpellAvaliblities.Add(enemy, new SpellAvaliblity(enemy));
            }
        }
    }

    
}