﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using EloBuddy;
using EloBuddy.SDK;
using SharpDX;

namespace AutoRift.Situation
{
    public static class Turrent
    {
        private const string SpawnTurrent = "Order5";
        private const string NexusTurrent = "Order4";
        private const string InhibitorTurrent = "Order3";
        private const string InnerTurrent = "Order2";
        private const string OutterTurrent = "Order1";

        public static readonly float TurrentsRange = 780 + Player.BoundingRadius;

        public static class Ally
        {
            public static List<Obj_AI_Turret> Turrents { get { return EntityManager.Turrets.Allies; } }

        }
        public static class Enemy
        {
            public static List<Obj_AI_Turret> Turrents { get { return EntityManager.Turrets.Enemies; } }
        }

        public static Tier TurrentTier(this Obj_AI_Turret turrent)
        {
            if(turrent.BaseSkinName.EndsWith(SpawnTurrent)) return Tier.Spawn;
            if (turrent.BaseSkinName.EndsWith(NexusTurrent)) return Tier.Nexus;
            if (turrent.BaseSkinName.EndsWith(InhibitorTurrent)) return Tier.Inhibitor;
            if (turrent.BaseSkinName.EndsWith(InnerTurrent)) return Tier.Inner;
            return Tier.Outter;
        }
        //Is Near Turrents Range
        public static bool IsNearAllyTurrent(this Vector3 pos, float range, params Tier[] tiers)
        {
            return Ally.Turrents.Any(x => x.IsInRange(pos, TurrentsRange + range) && (tiers == null || tiers.Any(i => i == x.TurrentTier())));
        }
        public static bool IsNearEnemyTurrent(this Vector3 pos, float range, params Tier[] tiers)
        {
            return Enemy.Turrents.Any(x => x.IsInRange(pos, TurrentsRange + range) && (tiers == null || tiers.Any(i => i == x.TurrentTier())));
        }
        public static bool IsNearTurrent(this Vector3 pos, float range, params Tier[] tiers)
        {
            return IsNearAllyTurrent(pos, range, tiers) || IsNearEnemyTurrent(pos, range, tiers);
        }

        //Is In Range of turrent
        public static bool IsInTurrent(this Vector3 pos, params Tier[] tiers)
        {
            return IsInAllyTurrent(pos, tiers) || IsInEnemyTurrent(pos, tiers);
        }
        public static bool IsInAllyTurrent(this Vector3 pos, params Tier[] tiers)
        {
            // Turrent is in range, and if tiers specifyed is it that tier
            return Ally.Turrents.Any(x => x.IsInRange(pos, TurrentsRange) && (tiers == null || tiers.Any(i => i == x.TurrentTier()))); 
        }
        public static bool IsInEnemyTurrent(this Vector3 pos, params Tier[] tiers)
        {
            // Turrent is in range, and if tiers specifyed is it that tier
            return Enemy.Turrents.Any(x => x.IsInRange(pos, TurrentsRange) && (tiers == null || tiers.Any(i => i == x.TurrentTier())));
        }

        public enum Tier
        {
            Outter, Inner, Inhibitor, Nexus, Spawn
        }
    }
}